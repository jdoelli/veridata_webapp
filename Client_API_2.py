##
import webbrowser
import msal
import requests
from msal import PublicClientApplication
##

APPLICATION_ID = 'c331f05c-f703-43d5-83b1-b22c98b58b6b'
CLIENT_SECRET = 'EVf8Q~bT-AwdXay71_93FSoyXPBex_us0za2Tcry'

base_url = 'https://graph.microsoft.com/v1.0/'
endpoint = base_url + 'me/drive/root/children'
authority_url = 'https://login.microsoftonline.com/consumers'

SCOPES = ['User.Read']
##

# method 2
app = PublicClientApplication(
    APPLICATION_ID,
    authority=authority_url
)

##

flow = app.initiate_device_flow(scopes=SCOPES)
print(flow)

print(flow['message'])
webbrowser.open(flow['verification_uri'])

result = app.acquire_token_by_device_flow(flow)

access_token_id = result['access_token']
headers = {'Authorization': 'Bearer ' + access_token_id}
response = requests.get(endpoint, headers=headers)
print(response.json())