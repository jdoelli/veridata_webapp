##
import webbrowser
import msal
import requests
from msal import PublicClientApplication
##

APPLICATION_ID = 'dbb6d111-88e9-438b-82d1-4b7071be731d'
CLIENT_SECRET = 'JLc8Q~tR3lIobAJrAoTmLKAwtnWQinUiRDI2gb8O'

base_url = 'https://graph.microsoft.com/v1.0/'
endpoint = base_url + 'me/drive/root/children'
authority_url = 'https://login.microsoftonline.com/consumers/'

SCOPES = ['Notes.Create', 'Notes.ReadWrite.All', 'User.Read']

# method 1
client_instance = msal.ConfidentialClientApplication(
    client_id=APPLICATION_ID,
    client_credential=CLIENT_SECRET,
    authority=authority_url
)

authorization_request_url = client_instance.get_authorization_request_url(SCOPES)
print(authorization_request_url)

webbrowser.open(authorization_request_url, new=True)
##
authorization_code = "M.C104_BL2.2.36e4555b-ed86-f580-a39f-75c8fe842ff8"

access_token = client_instance.acquire_token_by_authorization_code(
    code=authorization_code,
    scopes=SCOPES
)
##
access_token_id = access_token['access_token']
headers = {'Authorization': 'Bearer ' + access_token_id}
response = requests.get(endpoint, headers=headers)
print(response.json())
##