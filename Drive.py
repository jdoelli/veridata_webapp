import gdown
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive

""""
# Download file
URL = 'https://drive.google.com/drive/folders/1O-Y09OCzhqqXVcP7hkxZ1pT46bflkg8_?usp=share_link'
gdown.download_folder(URL, quiet=True, use_cookies=False)
"""

""""
# Upload file
gauth = GoogleAuth()
drive = GoogleDrive(gauth)

upload_file_list = ['./Excel/veri.data_tabletransfer.xlsm']
for upload_file in upload_file_list:
	gfile = drive.CreateFile({'parents': [{'id': '1O-Y09OCzhqqXVcP7hkxZ1pT46bflkg8_'}]})
	# Read file and set it as the content of this instance.
	gfile.SetContentFile(upload_file)
	gfile.Upload()
"""

gauth = GoogleAuth()
gauth.LocalWebserverAuth()
drive = GoogleDrive(gauth)

folder_id = '1O-Y09OCzhqqXVcP7hkxZ1pT46bflkg8_'
fileID = ''

# View all folders and file in your Google Drive - insert the ID for the folder to get the items in the folder
fileList = drive.ListFile({'q': "'1O-Y09OCzhqqXVcP7hkxZ1pT46bflkg8_' in parents and trashed=false"}).GetList()
for file in fileList:
  print('Title: %s, ID: %s' % (file['title'], file['id']))
  fileID = file['id']

#Delete File
file2 = drive.CreateFile({'id': fileID})
file2.Delete()  # Permanently delete the file.

#Upload File
file1 = drive.CreateFile({"mimeType": "*/*", "parents": [{"kind": "drive#fileLink", "id": folder_id}]})
file1.SetContentFile("./Excel/veri.data_tabletransfer.xlsm")
file1.Upload() # Upload the file.
print('Created file %s with mimeType %s' % (file1['title'], file1['mimeType']))



