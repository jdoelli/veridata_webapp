import os
import requests
from ms_graph import generate_access_token, GRAPH_API_ENDPOINT

APP_ID = 'e81de971-689c-4de8-8087-a3a1925fd123'
file_id = '01KYAJRWSMXV27ARU7Y5BL6234SBTFQP37'
SCOPES = ['Files.Read']

save_location = os.getcwd()

access_token = generate_access_token(app_id=APP_ID, scopes=SCOPES)
headers = {
    'Authorization': 'Bearer ' + access_token['access_token']
}

# grab the file name
response_file_info = requests.get(
    GRAPH_API_ENDPOINT + f'/me/drive/items/{file_id}',
    headers=headers,
    params={'select': 'name'}
)
file_name = response_file_info.json().get('name')

# download OneDrive file
response_file_content = requests.get(GRAPH_API_ENDPOINT + f'/me/drive/items{file_id}/content', headers=headers)
with open(os.path.join(save_location, file_name), 'wb') as _f:
    _f.write(response_file_content.content)