import streamlit as st  # pip install streamlit
import pandas as pd  # pip install pandas
import plotly.express as px  # pip install plotly-express
import base64  # Standard Python Module
from io import StringIO, BytesIO  # Standard Python Module
import openpyxl as xl

st.set_page_config(page_title='Veri.Data Trials')
st.title('Veri Data Experiment')

data_file = "Experiment.xlsx"

if data_file:

    wb = xl.load_workbook(data_file)

    ## Select sheet
    sheet_selector = st.sidebar.selectbox("Select sheet:",wb.sheetnames)     
    df = pd.read_excel(data_file,sheet_selector)
    st.markdown(f"### Currently Selected: `{sheet_selector}`")
    st.write(df)

    ## Columns Selecter
    columns = st.multiselect("Columns:",df.columns)
    df[columns]
