import onedrivesdk

redirect_uri = 'http://localhost:8501'
client_secret = 'FlD8Q~_4IJsrSC825lDUBxUtk75kDMBxX35hFaxY'
client_id='3f5c421f-6f12-433b-8bee-8bb03b83499a'
api_base_url='https://api.onedrive.com/v1.0/' # to do
scopes=['wl.signin', 'wl.offline_access', 'onedrive.readwrite']

http_provider = onedrivesdk.HttpProvider()
auth_provider = onedrivesdk.AuthProvider(
    http_provider=http_provider,
    client_id=client_id,
    scopes=scopes)

client = onedrivesdk.OneDriveClient(api_base_url, auth_provider, http_provider)
auth_url = client.auth_provider.get_auth_url(redirect_uri)
# Ask for the code
print('Paste this URL into your browser, approve the app\'s access.')
print('Copy everything in the address bar after "code=", and paste it below.')
print(auth_url)
code = raw_input('Paste code here: ')

client.auth_provider.authenticate(code, redirect_uri, client_secret)